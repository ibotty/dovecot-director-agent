use anyhow::{anyhow, ensure};
use base64::prelude::{Engine as _, BASE64_STANDARD};
use http::StatusCode;
use reqwest::header;

use crate::*;

#[derive(Debug)]
pub struct Credentials {
    pub doveadm_url: String,
    pub api_key: String,
}

fn authorization_header(creds: &Credentials) -> String {
    let encoded = &BASE64_STANDARD.encode(creds.api_key.as_str());
    format!("X-Dovecot-API {}", encoded)
}

pub async fn make_request<T>(creds: &Credentials, request: T) -> anyhow::Result<T::Response>
where
    T: Request,
{
    let tag = "tag";
    let req = RequestWrapper { request, tag };
    let client = reqwest::Client::new();
    let response = client
        .post(creds.doveadm_url.clone())
        .header(header::AUTHORIZATION, authorization_header(creds))
        .json(&vec![req])
        .send()
        .await?;
    let status = response.status();

    ensure!(
        status == StatusCode::OK,
        "doveadm got response {}: {}.",
        status,
        response.text().await?
    );

    let list: Vec<ResponseWrapper<_>> = response.json().await?;
    let resp = list
        .into_iter()
        .next()
        .ok_or_else(|| anyhow!("Doveadm API did not return any result."))?;

    ensure!(
        resp.tag == tag,
        "Doveadm API did return tag {}, instead of {}",
        resp.tag,
        tag
    );

    resp.response.map_err(Into::into)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_credentials() -> Credentials {
        Credentials {
            doveadm_url: "http://localhost:8080/doveadm/v1".into(),
            api_key: "rxRS1OBZvoapgBkjsbDBfRDSxTWka5nDlpCTa7aAovgXzsJl71HH6MU5zNaz0YXtCpxcnDXq4ldrDKCyIuyIknuif1m9l9j4xNoY9D9DCwmbVrTBKc9ak9CSxZP8zl2o74dHuwVduXbQuOQ5cDHeHyyL1ypBKG2a0drgcdts2nq0usxvxJo1HY9lq4JNUn9V2dERmqoGoZvP0ioDA3zPZOuOardXU3lANFn0pYx7wCp3MFC9SiK87SUviCJNZyvp".into(),
        }
    }

    #[tokio::test]
    async fn proxylist_is_legit() {
        let resp = make_request(&test_credentials(), proxy_list_request())
            .await
            .unwrap();
        assert_ne!(resp.len(), 0);
        assert!(resp.into_iter().any(|r| r.username == "tf@schaeffer-ag.de"));
    }

    #[test]
    fn authorization_header_correct() {
        let golden = "X-Dovecot-API cnhSUzFPQlp2b2FwZ0JranNiREJmUkRTeFRXa2E1bkRscENUYTdhQW92Z1h6c0psNzFISDZNVTV6TmF6MFlYdENweGNuRFhxNGxkckRLQ3lJdXlJa251aWYxbTlsOWo0eE5vWTlEOURDd21iVnJUQktjOWFrOUNTeFpQOHpsMm83NGRIdXdWZHVYYlF1T1E1Y0RIZUh5eUwxeXBCS0cyYTBkcmdjZHRzMm5xMHVzeHZ4Sm8xSFk5bHE0Sk5VbjlWMmRFUm1xb0dvWnZQMGlvREEzelBaT3VPYXJkWFUzbEFORm4wcFl4N3dDcDNNRkM5U2lLODdTVXZpQ0pOWnl2cA==";
        let generated = authorization_header(&test_credentials());

        assert_eq!(golden, generated);
    }
}
