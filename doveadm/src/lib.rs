use paste::paste;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;
use std::collections::HashMap;

mod requests;
mod types;
pub use crate::types::*;
pub use crate::requests::*;

macro_rules! request_struct {
($name:ident, $($param:ident : $ty:ty),*) => { paste!(
    #[serde_with::apply(
        Option => #[serde(skip_serializing_if = "Option::is_none")],
        Vec => #[serde(skip_serializing_if = "Vec::is_empty")],
        HashMap => #[serde(skip_serializing_if = "HashMap::is_empty")],
    )]
    #[derive(Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct [<$name:camel Req>] {
        $(
            pub $param: $ty),*
    }
    pub fn [<$name:snake _request>]($($param:$ty),*) -> [<$name:camel Req>] {
        [<$name:camel Req>]{ $($param),* }
    }
);}}

macro_rules! response_struct {
($name:ident, $($param:ident : $ty:ty),*) => { paste!(
    #[skip_serializing_none]
    #[derive(Debug, Deserialize, PartialEq, Eq)]
    #[serde(rename_all = "kebab-case")]
    pub struct [<$name:camel>] {
        $(pub $param: $ty),*
    }
);}}

macro_rules! request_impl {
($name:ident, $req_ty:ty, $resp_ty:ty) => { paste!(
    impl Request for $req_ty {
        type Response = $resp_ty;
        const SERIALIZED_NAME: &'static str = stringify!($name);
    }
);}}

macro_rules! calling_function {
// ($name:ident, $($param:ident : $ty:ty),*) => { paste!(
($name:ident, $req_name:ident {$($param:ident : $ty:ty),*}, $resp_ty:ty) => {
    pub async fn $name(creds: &Credentials, $($param: $ty),* ) -> anyhow::Result<$resp_ty> {
        make_request(creds, $req_name { $($param),* }).await
    }
}}

macro_rules! make_types_multiple {
($name:ident { $($req_param:ident : $req_ty:ty),* } { $($resp_param:ident : $resp_ty:ty),* }) => {
    request_struct!($name, $( $req_param: $req_ty),*);
    response_struct!($name, $( $resp_param: $resp_ty),*);
    paste!(request_impl!($name, [<$name:camel Req>], Vec<[<$name:camel>]>););
    paste!(calling_function!([<$name:snake>], [<$name:camel Req>] {$($req_param: $req_ty),* }, Vec<[<$name:camel>]>););
}}

macro_rules! make_types {
($name:ident { $($req_param:ident : $req_ty:ty),* } { $($resp_param:ident : $resp_ty:ty),* }) => {
    request_struct!($name, $( $req_param: $req_ty),*);
    response_struct!($name, $( $resp_param: $resp_ty),*);
    paste!(request_impl!($name, [<$name:camel Req>], [<$name:camel>]););
    paste!(calling_function!([<$name:snake>], [<$name:camel Req>] {$($req_param: $req_ty),* }, [<$name:camel>]););
};
($name:ident { $($req_param:ident : $req_ty:ty),* } $resp_ty:ty) => {
    request_struct!($name, $( $req_param: $req_ty),*);
    paste!(request_impl!($name, [<$name:camel Req>], $resp_ty););
}}

// TODO: serialize as `["key=value", ..]`
#[derive(Serialize)]
pub struct AuthInfoReq {
    service: Option<String>,
    lip: Option<String>,
    rip: Option<String>,
    lport: Option<u16>,
    rport: Option<u16>,
}

make_types!(user
{
    auth_info: Vec<AuthInfoReq>,
    field: Option<String>,
    expand_field: Option<String>,
    userdb_only: Option<bool>,
    user_mask: Vec<String>
}
HashMap<String, HashMap<String,String>>
);

make_types!(reload {} Vec<()>);

make_types_multiple!(who {separate_connections: Option<bool>, mask: Option<String>}
{   connections: String,
    ips: String,
    pids: String,
    service: String,
    username: String
});

make_types_multiple!(
proxyList
    {}
    {
        dest_ip: String,
        dest_port: String,
        service: String,
        src_ip: String,
        username: String
    }
);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_error() {
        let deserialized_golden = ResponseError::UserDoesNotHaveSession;
        let deserialized_string = r#"
                {
                    "exitCode": 68,
                    "type": "exitCode"
                }
                "#;
        let deserialized: ResponseError = serde_json::from_str(deserialized_string).unwrap();
        assert_eq!(deserialized, deserialized_golden);
    }

    #[test]
    fn deserialize_untagged_error() {
        let deserialized_golden = UntaggedResult::Err(ResponseError::UserDoesNotExist);
        let deserialized_string = r#"
                {
                    "exitCode": 67,
                    "type": "exitCode"
                }
                "#;
        let deserialized: UntaggedResult<(), ResponseError> =
            serde_json::from_str(deserialized_string).unwrap();
        assert_eq!(deserialized, deserialized_golden);
    }

    #[test]
    fn deserialize_wrapper_error() {
        let deserialized_golden: ResponseWrapper<()> = ResponseWrapper {
            tag: "tag1".to_string(),
            response: Err(ResponseError::UserQuotaIsFull),
        };
        let deserialized_string = r#"
            [
                "error",
                {
                    "exitCode": 73,
                    "type": "exitCode"
                },
                "tag1"
            ]"#;
        let deserialized: Result<ResponseWrapper<_>, _> =
            serde_json::from_str(deserialized_string).map_err(|e| e.to_string());
        assert_eq!(deserialized, Ok(deserialized_golden));
    }

    #[test]
    fn deserialize_user_resp() {
        let deserialized_golden: ResponseWrapper<HashMap<_, HashMap<_, _>>> = ResponseWrapper {
            tag: "tag1".to_string(),
            response: Ok(HashMap::from([(
                "samik".to_string(),
                HashMap::from_iter([
                    ("gid", "1000"),
                    ("home", "/var/vmail/samik"),
                    ("junkflag", "0"),
                    ("mail", "Maildir:/mails/mails/samik/Maildir"),
                    ("namespace/Junk/hidden", "no"),
                    ("namespace/Junk/list", "yes"),
                    ("uid", "1000"),
                ]),
            )])),
        };

        let deserialized_string = r#"
            [
                "doveadmResponse",
                {
                    "samik": {
                        "gid": "1000",
                        "home": "/var/vmail/samik",
                        "junkflag": "0",
                        "mail": "Maildir:/mails/mails/samik/Maildir",
                        "namespace/Junk/hidden": "no",
                        "namespace/Junk/list": "yes",
                        "uid": "1000"
                    }
                },
                "tag1"
            ]"#;
        let deserialized: Result<ResponseWrapper<_>, _> =
            serde_json::from_str(deserialized_string).map_err(|e| e.to_string());
        assert_eq!(deserialized, Ok(deserialized_golden));
    }

    #[test]
    fn serialize_user_req() {
        let user_req = RequestWrapper {
            request: UserReq {
                auth_info: Default::default(),
                field: None,
                expand_field: None,
                userdb_only: None,
                user_mask: vec!["samik".to_string()],
            },
            tag: "tag1",
        };
        let serialized = serde_json::to_string(&user_req).unwrap();
        let serialized_golden = r#"["user",{"userMask":["samik"]},"tag1"]"#;
        assert_eq!(serialized, serialized_golden);
    }

    #[test]
    fn deserialize_proxy_list_req() {
        let deserialized_golden = ResponseWrapper {
            response: Ok(vec![ProxyList {
                dest_ip: "127.0.0.1".to_string(),
                dest_port: "1143".to_string(),
                service: "imap".to_string(),
                src_ip: "10.0.0.1".to_string(),
                username: "my_user@example.com".to_string(),
            }]),
            tag: "tag1".to_string(),
        };
        let deserialized_string = "[\"doveadmResponse\",[{\"dest-ip\": \"127.0.0.1\", \"dest-port\": \"1143\", \"service\": \"imap\", \"src-ip\": \"10.0.0.1\", \"username\": \"my_user@example.com\"}],\"tag1\"]";
        let deserialized: Result<ResponseWrapper<Vec<ProxyList>>, _> =
            serde_json::from_str(deserialized_string).map_err(|e| e.to_string());
        assert_eq!(deserialized, Ok(deserialized_golden));
    }

    #[test]
    fn serialize_proxy_list_resp() {
        let proxy_list_req = RequestWrapper {
            request: proxy_list_request(),
            tag: "tag1",
        };
        let serialized = serde_json::to_string(&proxy_list_req).unwrap();
        let serialized_golden = "[\"proxyList\",{},\"tag1\"]";
        assert_eq!(serialized, serialized_golden);
    }

    #[test]
    fn serialize_reload_req() {
        let reload_req = RequestWrapper {
            request: reload_request(),
            tag: "tag1",
        };
        let serialized = serde_json::to_string(&reload_req).unwrap();
        let serialized_golden = "[\"reload\",{},\"tag1\"]";
        assert_eq!(serialized, serialized_golden);
    }
}
