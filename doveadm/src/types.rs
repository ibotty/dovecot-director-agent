use serde::de::{self, Deserializer};
use serde::ser::{SerializeTuple, Serializer};
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub trait Request: Serialize
where
    <Self as Request>::Response: for<'a> Deserialize<'a>,
{
    type Response;
    const SERIALIZED_NAME: &'static str;
}

#[derive(Debug, PartialEq, Eq)]
pub struct RequestWrapper<'a, T> {
    pub request: T,
    pub tag: &'a str,
}

impl<'a, T> Serialize for RequestWrapper<'a, T>
where
    T: Serialize,
    T: Request,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut tup = serializer.serialize_tuple(3)?;

        tup.serialize_element(&T::SERIALIZED_NAME)?;
        tup.serialize_element(&self.request)?;
        tup.serialize_element(self.tag)?;
        tup.end()
    }
}

#[derive(Debug, Error, Eq, PartialEq, Deserialize)]
#[serde(from = "ResponseErrorWrapper")]
pub enum ResponseError {
    #[error("Success but mailbox changed during operation")]
    SuccessButMailboxChanged,
    #[error("Invalid parameters")]
    InvalidParameters,
    #[error("Data error")]
    DataError,
    #[error("User does not exist")]
    UserDoesNotExist,
    #[error("User does not have session")]
    UserDoesNotHaveSession,
    #[error("User quota is full")]
    UserQuotaIsFull,
    #[error("Temporary error")]
    TemporaryError,
    #[error("No permission")]
    NoPermission,
    #[error("Invalid configuration")]
    InvalidConfiguration,
    #[error("InternalError: Unknown doveadm error type: {}", "_0")]
    UnknownErrorType(String),
    #[error("InternalError: Unknown doveadm error code: {}", "_0")]
    UnknownErrorCode(u8),
}

#[derive(Debug, Eq, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ResponseErrorWrapper {
    #[serde(rename = "type")]
    ty: String,
    exit_code: u8,
}

impl From<ResponseErrorWrapper> for ResponseError {
    fn from(ResponseErrorWrapper { ty, exit_code }: ResponseErrorWrapper) -> Self {
        if ty != "exitCode" {
            ResponseError::UnknownErrorType(ty)
        } else {
            match exit_code {
                2 => ResponseError::SuccessButMailboxChanged,
                64 => ResponseError::InvalidParameters,
                65 => ResponseError::DataError,
                67 => ResponseError::UserDoesNotExist,
                68 => ResponseError::UserDoesNotHaveSession,
                73 => ResponseError::UserQuotaIsFull,
                75 => ResponseError::TemporaryError,
                77 => ResponseError::NoPermission,
                78 => ResponseError::InvalidConfiguration,
                e => ResponseError::UnknownErrorCode(e),
            }
        }
    }
}

#[derive(Debug, Deserialize, Eq, PartialEq, PartialOrd)]
#[serde(untagged)]
pub enum UntaggedResult<T, E> {
    Ok(T),
    Err(E),
}

impl<T, E> From<UntaggedResult<T, E>> for Result<T, E> {
    fn from(e: UntaggedResult<T, E>) -> Self {
        match e {
            UntaggedResult::Ok(t) => Result::Ok(t),
            UntaggedResult::Err(e) => Result::Err(e),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct ResponseWrapper<T> {
    pub response: Result<T, ResponseError>,
    pub tag: String,
}

impl<'de, T> Deserialize<'de> for ResponseWrapper<T>
where
    T: Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Deserialize::deserialize(deserializer).and_then(
            |(response_type, response_untagged, tag): (
                &str,
                UntaggedResult<T, ResponseError>,
                String,
            )| {
                let response: Result<T, ResponseError> = Into::into(response_untagged);
                match response_type {
                    "doveadmResponse" => Ok(ResponseWrapper { response, tag }),
                    "error" => Ok(ResponseWrapper { response, tag }),
                    _ => Err(de::Error::custom("unknown doveadm response type")),
                }
            },
        )
    }
}
