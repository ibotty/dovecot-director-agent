-- Dovecot auxilary lua passdb script for proxy setups
--
-- It is part of dovecot-kubernetes-agent.

-- disable globals
dovecot.restrict_global_variables(true)

local lb_webhook_url = "http://localhost:12345/user_backend_map"
local http_client = dovecot.http.client({ user_agent="dovecot-lb-passdb" })

-- -- failsafe
-- function auth_passdb_lookup(req)
--     return dovecot.auth.PASSDB_RESULT_OK, "proxy=y host=" .. "dovecot-backend"
-- end

function auth_passdb_lookup(auth_request)
    local user = auth_request.user
    local host = "dovecot-backend"

    local req = http_client:request({ url=lb_webhook_url; method="POST" })
    req:set_payload('{"username": "' .. user .. '"}') 
    local resp = req:submit()

    if resp:status() == 200
    then
	host = resp:payload()
        auth_request:log_info("auto-proxy.lua: user " .. user .. " redirect to host=" .. host)
        return dovecot.auth.PASSDB_RESULT_OK, "proxy=y host=" .. host
    else
        auth_request:log_warning("auto-proxy.lua: cannot get backend for user " .. user)
        -- return dovecot.auth.PASSDB_RESULT_INTERNAL_FAILURE

	-- for now redirect to default host
        auth_request:log_warning("auto-proxy.lua: http req failed.  User " .. user .. " redirect to default host=" .. host)
        return dovecot.auth.PASSDB_RESULT_OK, "proxy=y host=" .. host
    end
end
