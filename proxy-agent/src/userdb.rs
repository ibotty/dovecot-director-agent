use redis::AsyncCommands;
use std::borrow::Cow;

use crate::httpresponse::HttpError;

// TODO: maybe don't expire?  Let the backend handle it.
const BACKEND_MAP_TIMEOUT: usize = 60;
const BACKEND_OBJECTS_TTL_SECS: usize = 31;

fn user_backend_path(id: &str) -> String {
    format!("dovecot/users/{}/proxy_host", id)
}

fn backend_users_path(id: &str) -> String {
    format!("dovecot/backends/{id}/users")
}

pub async fn get_backends(mut redis_con: redis::aio::Connection) -> Result<String, HttpError> {
    redis_con
        .zrange("dovecot/backends/load_map", 0, 0)
        .await
        .map(|v: Vec<String>| v.first().unwrap().to_string())
        .map_err(|e| HttpError::Internal { err: e.to_string() })
}

pub async fn insert_user_backend<'a>(
    mut redis_con: redis::aio::Connection,
    username: &'a str,
    backend_candidate: &'a str,
) -> Result<String, String> {
    // set user's backend
    let backend = {
        let path = user_backend_path(username);

        // this will return one element, but redis-rs cannot deduce that and fails at runtime.
        let backend_list: Vec<String> = redis::pipe()
            .set(&path, backend_candidate)
            .arg("NX")
            .ignore()
            .expire(&path, BACKEND_MAP_TIMEOUT)
            .ignore()
            .get(&path)
            .query_async(&mut redis_con)
            .await
            .map_err(|e| format!("error setting user backend: {}", e))?;
        let backend = backend_list
            .first()
            .ok_or_else(|| format!("backend list empty for username {}", username))?;
        backend.to_string()
    };

    // add user to backend's user map
    let _ = {
        let backend_path = backend_users_path(&backend);
        redis::pipe()
            .sadd(&backend_path, username)
            .expiremember(&backend_path, username, BACKEND_OBJECTS_TTL_SECS)
            .query_async(&mut redis_con)
            .await
            .map_err(|e| {
                format!("error adding user {username} to backend {backend}'s user map: {e}")
            })?;
    };

    Ok(backend)
}

pub async fn get_user_backend(
    mut con: redis::aio::Connection,
    username: &str,
) -> Result<String, HttpError> {
    let path = user_backend_path(username);
    let res: Option<String> = con.get(path.clone()).await?;
    res.ok_or(HttpError::NotFound {
        msg: Cow::Borrowed(""),
    })
}
