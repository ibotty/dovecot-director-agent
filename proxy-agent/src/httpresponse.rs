use std::borrow::Cow;

use axum::response::{IntoResponse, Response};
use http::StatusCode;
use thiserror::Error;

pub type HttpResult = Result<Response, HttpError>;

pub trait IntoHttp {
    fn into_http(self) -> HttpResult;
}

#[derive(Debug, Error)]
pub enum HttpError {
    #[error("404 NotFound: {}", "_0")]
    NotFound { msg: Cow<'static, str> },
    #[error("500 Internal Error: {}", "_0")]
    Internal { err: String },
}

impl IntoResponse for HttpError {
    fn into_response(self) -> Response {
        match self {
            HttpError::NotFound { msg } => (StatusCode::NOT_FOUND, msg).into_response(),
            HttpError::Internal { err } => (StatusCode::INTERNAL_SERVER_ERROR, err).into_response(),
        }
    }
}

macro_rules! httperror_impl_from {
    ($from:ty) => {
        impl From<$from> for HttpError {
            fn from(err: $from) -> Self {
                HttpError::Internal {
                    err: err.to_string(),
                }
            }
        }
    };
}

httperror_impl_from!(std::io::Error);
httperror_impl_from!(anyhow::Error);
httperror_impl_from!(String);
httperror_impl_from!(redis::RedisError);
