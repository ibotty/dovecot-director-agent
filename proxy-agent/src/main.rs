use axum::{
    debug_handler,
    extract::{Json, Path, State},
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Router,
};
use http::header;
use serde::{Deserialize, Serialize};
use std::env;
use std::net::ToSocketAddrs;
use std::sync::Arc;

use doveadm;
mod httpresponse;
mod userdb;
use httpresponse::*;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let redis_connect_string =
        env::var("REDIS_URL").unwrap_or_else(|_| "redis://dovecot-keydb-service".to_string());
    let bind_addr = env::var("BIND_ADDR")
        .unwrap_or_else(|_| "localhost:12345".to_string())
        .to_socket_addrs()
        .unwrap()
        .next()
        .unwrap();

    let redis = redis::Client::open(redis_connect_string).unwrap();
    let shared_state = Arc::new(redis);

    let app = Router::new()
        .route("/user-backend-map/", post(post_user_backend))
        .route("/user-backend-map/:id", get(get_user_backend))
        .with_state(shared_state);

    axum::Server::bind(&bind_addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Serialize, Deserialize, Debug)]
struct UserBackendPayload {
    username: String,
}

/// create a new user backend connection
#[debug_handler]
async fn post_user_backend(
    State(redis): State<Arc<redis::Client>>,
    Json(payload): Json<UserBackendPayload>,
) -> HttpResult {
    let username = payload.username;
    let redis_con = redis.get_async_connection().await?;
    let backend_candidate = userdb::get_backends(redis_con).await?;
    let redis_con = redis.get_async_connection().await?;
    let backend = userdb::insert_user_backend(redis_con, &username, &backend_candidate).await?;

    Ok((
        StatusCode::CREATED,
        [(header::LOCATION, format!("/user_backend_map/{}", username))],
        format!("{}\n", backend),
    )
        .into_response())
}

/// return the ip for the user's backend
#[debug_handler]
async fn get_user_backend(
    State(redis): State<Arc<redis::Client>>,
    Path(id): Path<String>,
) -> HttpResult {
    let con = redis.get_async_connection().await.unwrap();
    userdb::get_user_backend(con, &id)
        .await
        .map(|b| b.into_response())
}
