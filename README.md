# Dovecot Operator

## WIP

Not everything is implemented yet.

 * on proxy a (e.g.) redis userdb that sets the user's backend server,
 * an agent that monitors backends and updates the userdb,
 * a balancer that redistributes imap users when the set of backends changes (kicking old sessions).

## Backend shutdown ordering

The `dovecot` container in the `dovecot-backend` pod should have a `preStop` hook that checks whether the `backend-agent` sidecar has already stopped.

This is neccessary to allow seamless moving of users to the new backend.

## Moving Users between backends

See <https://doc.dovecot.org/configuration_manual/authentication/proxies/#moving-users-between-backends-clusters>.
