# KeyDB Usage

## Backend List

The backends are registered by the backend-agent.

They will use the following keys.

 * `dovecot/backends/<IP>/users` is a SET of user names, expiring after BACKEND_EXPIRE_TIME,
 * `dovecot/backends/load_map` is a ZSET with backend IP weighted by the backend's load, "account", the backends entry expiring after BACKEND_EXPIRE_TIME.

The load_map expiration requires KeyDB's `EXPIREMEMBER` command.

## User 

 * `dovecot/users/<USERID>/delay_until`, if set will delay the user when migrating, expiring after USER_SESSION_MOVE_DELAY,
 * `dovecot/users/<USERID>/proxy_host`, the backend IP to which the dovecot proxys direct the user, expiring after BACKEND_EXPIRE_TIME.
