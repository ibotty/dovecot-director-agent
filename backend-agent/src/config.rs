use std::env;
use std::str::FromStr;
use std::sync::LazyLock;
use std::time::Duration;

fn get_env_and_parse<T>(env_name: &str, default: T) -> T
where
    T: FromStr,
{
    env::var(env_name)
        .ok()
        .and_then(|v| FromStr::from_str(&v).ok())
        .unwrap_or(default)
}

fn get_env_and_fail(env_name: &str) -> String {
    env::var(env_name)
        .ok()
        .and_then(|v| FromStr::from_str(&v).ok())
        .unwrap_or_else(|| panic!("Environment variable {env_name} not set!"))
}

pub static SLEEP_INTERVAL: LazyLock<u64> =
    LazyLock::new(|| get_env_and_parse("DOVECOT_AGENT_SLEEP_INTERVAL", 30_000));

pub static USER_SESSION_MOVE_DELAY: LazyLock<Duration> = LazyLock::new(|| {
    Duration::from_secs(get_env_and_parse(
        "DOVECOT_AGENT_USER_SESSION_MOVE_DELAY",
        31,
    ))
});

pub static REDIS_CONNECT_STRING: LazyLock<String> = LazyLock::new(|| get_env_and_fail("REDIS_URL"));

pub static BACKEND_IP: LazyLock<String> = LazyLock::new(|| get_env_and_fail("BACKEND_IP"));

pub static DOVECOT_CREDS: LazyLock<doveadm::Credentials> = LazyLock::new(|| doveadm::Credentials {
    doveadm_url: get_env_and_parse("DOVEADM_API_URL", "http://localhost:8080/doveadm/v1".into()),
    api_key: get_env_and_fail("DOVEADM_API_KEY"),
});
