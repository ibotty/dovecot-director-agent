use redis::AsyncCommands;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tokio::try_join;

const DELAY_MAX_RANDOM_SECS: u64 = 3;
const BACKEND_OBJECTS_TTL_SECS: usize = 31;

const LOAD_MAP_PATH: &str = "dovecot/backends/load_map";

fn user_delay_path(id: &str) -> String {
    format!("dovecot/users/{id}/delay_until")
}

fn user_backend_path(id: &str) -> String {
    format!("dovecot/users/{id}/proxy_host")
}

fn backend_users_path(id: &str) -> String {
    format!("dovecot/backends/{id}/users")
}

pub(crate) async fn set_backend_load(
    redis: &redis::Client,
    backend: &str,
    score: u16,
) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    let mut pipe = redis::pipe();
    pipe.atomic()
        .zadd(LOAD_MAP_PATH, backend, score)
        .expiremember(LOAD_MAP_PATH, backend, BACKEND_OBJECTS_TTL_SECS)
        .query_async(&mut redis_con)
        .await?;
    Ok(())
}

pub(crate) async fn remove_backend_load(
    redis: &redis::Client,
    backend: &str,
) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    Ok(redis_con.zrem(LOAD_MAP_PATH, backend).await?)
}

pub(crate) async fn remove_backend_users_map(
    redis: &redis::Client,
    backend: &str,
) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    Ok(redis_con.del(backend_users_path(backend)).await?)
}

pub(crate) async fn delay_user_session(
    redis: &redis::Client,
    user: &str,
    delay: Duration,
) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    let path = user_delay_path(user);
    let until = (SystemTime::now() + delay).duration_since(UNIX_EPOCH)?;
    let delay_str = format!("{}+{}", until.as_secs(), DELAY_MAX_RANDOM_SECS);
    Ok(redis_con
        .set_ex(
            path,
            delay_str,
            (delay.as_secs() + DELAY_MAX_RANDOM_SECS).try_into()?,
        )
        .await?)
}

pub(crate) async fn remove_user_backend(redis: &redis::Client, user: &str) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    let path = user_backend_path(user);
    Ok(redis_con.del(path).await?)
}

pub(crate) async fn set_backend_users_map(
    redis: &redis::Client,
    backend: &str,
    users: &Vec<String>,
) -> anyhow::Result<()> {
    let mut redis_con = redis.get_async_connection().await?;
    let backend_path = backend_users_path(backend);
    let mut pipe = redis::pipe();
    let task1 = pipe
        .atomic()
        .del(&backend_path) // delete all users
        .sadd(&backend_path, users) // and add new users
        .query_async(&mut redis_con);

    let mut redis_con = redis.get_async_connection().await?;
    let args: Vec<(String, String)> = users
        .iter()
        .map(|u| (user_backend_path(u), backend.to_string()))
        .collect();
    let task2 = redis_con.set_multiple(&args);

    let _: ((), ()) = try_join!(task1, task2)?;

    // set expire for the backend-users map
    let mut redis_con = redis.get_async_connection().await?;
    redis_con
        .expire(&backend_path, BACKEND_OBJECTS_TTL_SECS)
        .await?;

    Ok(())
}

pub(crate) async fn users_on_backend<'a>(
    redis_con: &'a mut redis::aio::Connection,
    backend: &str,
) -> anyhow::Result<redis::AsyncIter<'a, String>> {
    let path = backend_users_path(backend);
    Ok(redis_con.sscan(path).await?)
}
