use tokio::sync::watch;

// modeled after https://github.com/tokio-rs/mini-redis/blob/master/src/shutdown.rs

pub(crate) struct Shutdown {
    should_shutdown: bool,
    
    /// The receive half of the channel used to listen for shutdown.
    notify: watch::Receiver<()>,
}

impl Shutdown {
    pub(crate) fn new(notify: watch::Receiver<()>) -> Shutdown {
        Shutdown { should_shutdown: false, notify }
    }

    pub(crate) fn should_shutdown(&self) -> bool {
        self.should_shutdown
    }

    pub(crate) async fn recv(&mut self) {
        // If the shutdown signal has already been received, then return
        // immediately.
        if self.should_shutdown {
            return;
        }

        // Cannot receive a "lag error" as only one value is ever sent.
        let _ = self.notify.changed().await;
        
        // Remember that the signal has been received.
        self.should_shutdown = true;
    }
}
