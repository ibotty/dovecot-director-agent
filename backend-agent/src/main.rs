#![feature(once_cell)]
#![feature(result_option_inspect)]

use std::sync::Arc;
use tokio::signal::unix::SignalKind;
use tokio::task::JoinHandle;
use tokio::time::Duration;
use tokio::{join, signal};
use tokio_retry::{
    strategy::{jitter, FibonacciBackoff},
    Retry,
};

mod backenddb;
mod config;
mod dovecot;

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let redis = Arc::new(redis::Client::open(config::REDIS_CONNECT_STRING.clone())?);

    let tasks = vec![
        spawn_backend_load_task(redis.clone()).await,
        spawn_user_list_task(redis.clone()).await,
    ];

    // shutdown handling
    let mut terminate_signals = signal::unix::signal(SignalKind::terminate())?;
    tokio::select! {
        _ = signal::ctrl_c() => {},
        _ = terminate_signals.recv() => {},
    };

    // abort all tasks
    tasks.into_iter().for_each(|t| t.abort());

    let _ = remove_backend(redis.clone()).await?;
    Ok(())
}

async fn spawn_backend_load_task(redis: Arc<redis::Client>) -> JoinHandle<anyhow::Result<()>> {
    let retry_strategy = FibonacciBackoff::from_millis(100)
        .max_delay(Duration::from_secs(60))
        .map(jitter);
    tokio::spawn(Retry::spawn(retry_strategy, move || {
        backend_load_task(redis.clone())
    }))
}

async fn spawn_user_list_task(redis: Arc<redis::Client>) -> JoinHandle<anyhow::Result<()>> {
    let retry_strategy = FibonacciBackoff::from_millis(100)
        .max_delay(Duration::from_secs(60))
        .map(jitter);
    tokio::spawn(Retry::spawn(retry_strategy, move || {
        user_list_task(redis.clone())
    }))
}

async fn user_list_task(redis: Arc<redis::Client>) -> anyhow::Result<()> {
    loop {
        let backend = &(*config::BACKEND_IP);

        println!("Get user list from dovecot");
        let users_res = dovecot::get_users(&config::DOVECOT_CREDS).await;
        println!("Get user list from dovecot: {users_res:?}");
        let users = users_res?;

        println!("Set backend-user map {backend}");
        backenddb::set_backend_users_map(&redis, backend, &users).await?;

        tokio::time::sleep(Duration::from_millis(*config::SLEEP_INTERVAL)).await;
    }
}

async fn backend_load_task(redis: Arc<redis::Client>) -> anyhow::Result<()> {
    let score = dovecot::get_backend_score(&config::DOVECOT_CREDS).await?;
    loop {
        let backend = &(*config::BACKEND_IP);
        println!("Set backend {backend}'s load to {score}");
        backenddb::set_backend_load(&redis, backend, score).await?;

        tokio::time::sleep(Duration::from_millis(*config::SLEEP_INTERVAL)).await;
    }
}

// Order is important to not have a race condition.
// First remove the backend so no new clients can connect,
// then delay the backend's user's new sessions,
// finally remove the backend's user map.
async fn remove_backend(redis: Arc<redis::Client>) -> anyhow::Result<()> {
    println!("remove backend from backend load map in redis.");
    let _ = backenddb::remove_backend_load(&redis, &config::BACKEND_IP)
        .await
        .inspect_err(|e| println!("cannot remove backend_load: {e}"));

    let mut redis_con = redis.get_async_connection().await?;
    let mut users_iter = backenddb::users_on_backend(&mut redis_con, &config::BACKEND_IP).await?;

    while let Some(user) = users_iter.next_item().await {
        println!("removing proxy-host and blocking new user sessions for {user}");
        let (delay_res, rm_user_res) = join!(
            backenddb::delay_user_session(&redis, &user, *config::USER_SESSION_MOVE_DELAY),
            backenddb::remove_user_backend(&redis, &user)
        );
        let _ =
            delay_res.inspect_err(|e| println!("cannot delay user session for user {user}: {e:?}"));
        let _ = rm_user_res.inspect_err(|e| {
            println!("cannot delete user proxy-host mapping for user {user}: {e:?}")
        });
    }

    println!("removing backend's user map in redis.");
    let _ = backenddb::remove_backend_users_map(&redis, &config::BACKEND_IP)
        .await
        .inspect_err(|e| println!("cannot remove backend's user map: {e}"));
    Ok(())
}
