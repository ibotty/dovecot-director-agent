pub(crate) async fn get_users(creds: &doveadm::Credentials) -> anyhow::Result<Vec<String>> {
    let who_result = doveadm::who(creds, None, None).await?;
    println!("users: {who_result:?}");
    Ok(who_result.into_iter().map(|e| e.username).collect())
}

pub(crate) async fn get_backend_score(_creds: &doveadm::Credentials) -> anyhow::Result<u16> {
    Ok(1)
}

#[cfg(test)]
mod tests {
    use super::*;
    use doveadm::Credentials;

    fn test_credentials() -> Credentials {
        Credentials {
            doveadm_url: "http://localhost:8080/doveadm/v1".into(),
            api_key: "rxRS1OBZvoapgBkjsbDBfRDSxTWka5nDlpCTa7aAovgXzsJl71HH6MU5zNaz0YXtCpxcnDXq4ldrDKCyIuyIknuif1m9l9j4xNoY9D9DCwmbVrTBKc9ak9CSxZP8zl2o74dHuwVduXbQuOQ5cDHeHyyL1ypBKG2a0drgcdts2nq0usxvxJo1HY9lq4JNUn9V2dERmqoGoZvP0ioDA3zPZOuOardXU3lANFn0pYx7wCp3MFC9SiK87SUviCJNZyvp".into(),
        }
    }

    #[tokio::test]
    async fn who_is_legit() {
        let resp = get_users(&test_credentials()).await.unwrap();
        assert_ne!(resp.len(), 0);
        assert!(resp.into_iter().any(|r| r == "tf@schaeffer-ag.de"));
    }
}
